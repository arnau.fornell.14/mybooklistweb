import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Libro } from '../models/libro.model';

@Injectable({providedIn: 'root'})
export class LibrosService {

  constructor(private http: HttpClient) {}

  getLibrosPopulares() {
    return this.http.get<{ resultado: Array<{ id: number, titulo_original: string, portada: string, isbn: string}> }>('http://localhost:3000/getlibros');
  }

  getLibrosNovedades() {
    return this.http.get<{ resultado: Array<{ id: number, titulo_original: string, portada: string, isbn: string }> }>('http://localhost:3000/getnovedades');
  }

  getLibro(isbn: string) {
    return this.http.get<{ resultado: Libro, valido: boolean }>('http://localhost:3000/getfichalibro/' + isbn);
  }

  anyadirLibroLista(id: number, idLibro: number) {
    const variables = {
      usuario: id,
      id_libro: idLibro
    }
    return this.http.post<{mensaje: string}>('http://localhost:3000/user/anadirLibroLista', variables);
  }
}

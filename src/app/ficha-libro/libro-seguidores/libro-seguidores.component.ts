import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Subscription } from 'rxjs';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { LibroSeguidoresService } from 'src/app/services/libroseguidores.service'
import { LibroSeguidores } from 'src/app/models/libroseguidores.model';

@Component({
  selector: 'app-libro-seguidores',
  templateUrl: './libro-seguidores.component.html',
  styleUrls: ['./libro-seguidores.component.css']
})
export class LibroSeguidoresComponent implements OnInit {

  subscription: Subscription;
  isLogin: boolean = false;
  form: FormGroup;
  libroSeguidores: LibroSeguidores;
  @Input() isbn;

  constructor(private usuarioService: UsuarioService, private libroSeguidoresService: LibroSeguidoresService) { }

  ngOnInit(): void {
    this.isLogin = this.usuarioService.usuario? true: false;
    if(this.isLogin) {
      this.libroSeguidoresService.getLibrosSeguidores(this.isbn).subscribe( resultado => {
        this.libroSeguidores = resultado;
      });
    }

    //Listener para comprobar si el usuario se ha logueado dentro de este componente.
    this.usuarioService.usuarioLogin.subscribe(login => {
      this.isLogin = login;
      this.libroSeguidoresService.getLibrosSeguidores(this.isbn).subscribe( resultado => {
        this.libroSeguidores = resultado;
      });
    });
  }
}

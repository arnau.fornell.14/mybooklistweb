export interface Lista {
  id_estado: number;
  id_libro: number;
  estado: string[];
  isbn: string;
  titulo_original: string;
  portada: string;
  puntuacion: number;
  forumularioEstadoVisible: boolean;
}

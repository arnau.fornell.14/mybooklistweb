import { Component, OnInit, Input } from '@angular/core';
import { LibrosService } from 'src/app/services/libros.service';

@Component({
  selector: 'app-carousel-libros',
  templateUrl: './carousel-libros.component.html',
  styleUrls: ['./carousel-libros.component.css']
})
export class CarouselLibrosComponent implements OnInit {

  @Input() tipo: string = '';
  tituloCarousel = '';

  public libros: { id: number, titulo_original: string, portada: string, isbn: string } [] = [];

  constructor(private librosService: LibrosService) { }

  ngOnInit(): void {
    if (this.tipo === 'populares') {
      this.tituloCarousel = 'Libros populares'
      this.librosService.getLibrosPopulares().subscribe(respuesta => {
        this.libros = respuesta.resultado;
      });
    }
    if (this.tipo === 'novedades') {
      this.tituloCarousel = 'Novedades'
      this.librosService.getLibrosNovedades().subscribe(respuesta => {
        this.libros = respuesta.resultado;
      });
    }
  }
}

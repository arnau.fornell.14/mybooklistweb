import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { Analisis } from '../models/analisis.model';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AnalisisService {

  recargarAnalisis = new Subject<number>();

  constructor(private http: HttpClient) {}

  enviarAnalisis(id_libro: number, contenido: string) {
    const datos = {id_libro: id_libro, contenido: contenido}
    return this.http.post<{mensaje: string}>('http://localhost:3000/user/crearAnalisis', datos);
  }

  getAnalisis(id_libro: number) {
    const datos = {id_libro: id_libro}
    return this.http.post<{resultado: any}>('http://localhost:3000/user/verAnalisis', datos);
  }
}

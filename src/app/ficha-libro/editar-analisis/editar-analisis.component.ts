import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Subscription } from 'rxjs';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { AnalisisService } from 'src/app/services/analisis.service';

@Component({
  selector: 'app-editar-analisis',
  templateUrl: './editar-analisis.component.html',
  styleUrls: ['./editar-analisis.component.css']
})
export class EditarAnalisisComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  isLogin: boolean = false;
  form: FormGroup;
  enviado: boolean = false;
  @Input() idLibro;

  constructor(private usuarioService: UsuarioService, private analisisService: AnalisisService) { }

  ngOnInit(): void {
    this.isLogin = this.usuarioService.usuario ? true : false;
    this.subscription = this.usuarioService.usuarioLogin.subscribe( (login) => {
      this.isLogin = login;
    });
    this.form = new FormGroup({
      analisis: new FormControl(null , { validators: [Validators.required, Validators.minLength(400), Validators.maxLength(5000)] }),
    });
  }

  onSubmit() {
    this.analisisService.enviarAnalisis(this.idLibro, this.form.get('analisis').value).subscribe( respuesta => {
      this.enviado = true;
      this.analisisService.recargarAnalisis.next(null);
    });
    this.form.reset({});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

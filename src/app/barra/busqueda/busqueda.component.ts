import { Component, OnInit } from '@angular/core';
import { BusquedaService } from 'src/app/services/busqueda.service';
import { Busqueda } from 'src/app/models/busqueda.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  resultadoBusqueda: Busqueda[] = [];

  constructor(private busquedaService: BusquedaService, private router: Router) { }

  ngOnInit(): void {
  }

  onBusqueda(busqueda: string) {
    if(busqueda) {
      this.busquedaService.getBusqueda(busqueda).subscribe(respuesta => {
        this.resultadoBusqueda = respuesta.resultado;
      });
    }
    else {
      this.resultadoBusqueda = [];
    }
  }

  onResultado(busqueda: Busqueda) {
    if(busqueda.tipo === 1) {
      this.router.navigate(['/usuario', busqueda.nombre]);
    }
    else if(busqueda.tipo === 2) {
      this.router.navigate(['/libro', busqueda.isbn]);
    }
  }
}

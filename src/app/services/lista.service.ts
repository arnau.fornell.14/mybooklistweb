import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lista } from '../models/lista.model';

@Injectable({providedIn: 'root'})
export class ListaService {

  constructor(private http: HttpClient) {}

  getLista(usuario: string) {
    return this.http.get<{ resultado: Lista, estados: string[] }>('http://localhost:3000/user/listaLibros/' + usuario);
  }

  modificarLibroLista(id_libro: number, id_estado: number, puntuacion: number) {
    const datos = {id_libro: id_libro, id_estado: id_estado, puntuacion: puntuacion}
    return this.http.post<{mensaje: string}>('http://localhost:3000/user/modificarLibroLista', datos);
  }

  eliminarLibroLista(id_libro: number) {
    const datos = {id_libro: id_libro}
    return this.http.post<{mensaje: string}>('http://localhost:3000/user/borrarLibroLista', datos);
  }

  getAllLista(usuario: string){
    return this.http.get<{ resultado: Lista}>('http://localhost:3000/user/getAllListaLibros/' + usuario);
  }
}

DROP TABLE IF EXISTS Seguidores;
DROP TABLE IF EXISTS Actividad_API;
DROP TABLE IF EXISTS Perfil_Comentarios;
DROP TABLE IF EXISTS Actividad_Usuarios;
DROP TABLE IF EXISTS Actividad_Usuarios_Comentarios;
DROP TABLE IF EXISTS Tipos_Actividad_Usuarios;
DROP TABLE IF EXISTS Libros_Generos;
DROP TABLE IF EXISTS Personas_Roles_Libros;
DROP TABLE IF EXISTS Analisis;
DROP TABLE IF EXISTS Tokens;
DROP TABLE IF EXISTS Listas_Libros_Estados;
DROP TABLE IF EXISTS Estados;
DROP TABLE IF EXISTS Listas;
DROP TABLE IF EXISTS Noticias;
DROP TABLE IF EXISTS Generos;
DROP TABLE IF EXISTS Libros;
DROP TABLE IF EXISTS Personas;
DROP TABLE IF EXISTS Roles;
DROP TABLE IF EXISTS Editoriales;
DROP TABLE IF EXISTS Usuarios;
DROP TABLE IF EXISTS Empresas;


CREATE TABLE Empresas(
    id INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL,
    consultas BOOLEAN NOT NULL DEFAULT false,
    PRIMARY KEY(id),
    CONSTRAINT UC_email_empresas UNIQUE (email)
);

CREATE TABLE Usuarios(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario VARCHAR(50) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    apellidos VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL,
    contrasenya VARCHAR(255) NOT NULL,
    imagen VARCHAR(255),
    empresa INT(11),
    admin_empresa BOOLEAN,
    biografia TEXT,
    fecha_nacimiento DATE NOT NULL,
    fecha_registro TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    clave TEXT,
    fecha_ultimo_acceso TIMESTAMP,
    PRIMARY KEY(id),
    CONSTRAINT UC_usuario_usuarios UNIQUE (usuario),
    CONSTRAINT UC_email_empresas UNIQUE (email),
    FOREIGN KEY (empresa) REFERENCES Empresas(id) ON DELETE CASCADE
);

CREATE TABLE Editoriales(
    id INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Libros(
    id INT(11) NOT NULL AUTO_INCREMENT,
    titulo_ingles VARCHAR(255) NOT NULL,
    titulo_original VARCHAR(255) NOT NULL,
    isbn VARCHAR(13) NOT NULL,
    portada VARCHAR(255),
    fecha_publicacion DATE NOT NULL,
    sinopsis TEXT,
    puntuacion FLOAT(4,2) DEFAULT 0.0,
    editorial INT(11),
    PRIMARY KEY(id),
    FOREIGN KEY (editorial) REFERENCES Editoriales(id) ON DELETE SET NULL
);

CREATE TABLE Generos(
  id INT(11) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE Roles(
    id INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Personas(
    id INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    apellidos VARCHAR(255),
    foto VARCHAR(255),
    fecha_nacimiento DATE,
    PRIMARY KEY(id)
);

CREATE TABLE Noticias(
    id INT(11) NOT NULL AUTO_INCREMENT,
    titulo VARCHAR(255) NOT NULL,
    contenido TEXT NOT NULL,
    usuario INT(11),
    persona INT(11),
    libro INT(11),
    editorial INT(11),
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE SET NULL,
    FOREIGN KEY (persona) REFERENCES Personas(id) ON DELETE SET NULL,
    FOREIGN KEY (libro) REFERENCES Libros(id) ON DELETE SET NULL,
    FOREIGN KEY (editorial) REFERENCES Editoriales(id) ON DELETE SET NULL
);

CREATE TABLE Listas(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario INT(11) NOT NULL,
    publica BOOLEAN NOT NULL DEFAULT true,
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE CASCADE
);

CREATE TABLE Estados(
    id INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Listas_Libros_Estados(
    id INT(11) NOT NULL AUTO_INCREMENT,
    id_lista INT(11) NOT NULL,
    id_libro INT (11) NOT NULL,
    fecha_adicion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    id_estado INT(11),
    puntuacion FLOAT(4,2) DEFAULT 0.0,
    PRIMARY KEY(id),
    FOREIGN KEY (id_lista) REFERENCES Listas(id) ON DELETE CASCADE,
    FOREIGN KEY (id_libro) REFERENCES Libros(id) ON DELETE CASCADE,
    FOREIGN KEY (id_estado) REFERENCES Estados(id) ON DELETE SET NULL
);

CREATE TABLE Tokens(
    id INT(11) NOT NULL AUTO_INCREMENT,
    token TEXT NOT NULL,
    usuario INT(11) NOT NULL,
    fecha_emision TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fecha_caducidad TIMESTAMP NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE CASCADE
);

CREATE TABLE Analisis(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario INT(11) NOT NULL,
    id_libro INT(11) NOT NULL,
    contenido TEXT NOT NULL,
    fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fecha_ultima_edicion TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE CASCADE,
    FOREIGN KEY (id_libro) REFERENCES Libros(id) ON DELETE CASCADE
);

CREATE TABLE Tipos_Actividad_Usuarios(
    id INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Actividad_Usuarios(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario INT(11) NOT NULL,
    fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    tipo_actividad INT(11) NOT NULL,
    id_libro INT(11),
    id_noticia INT(11),
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE CASCADE,
    FOREIGN KEY (id_libro) REFERENCES Libros(id) ON DELETE CASCADE,
    FOREIGN KEY (id_noticia) REFERENCES Noticias(id) ON DELETE CASCADE
);

CREATE TABLE Actividad_Usuarios_Comentarios(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario INT(11) NOT NULL,
    contenido TEXT,
    actividad INT(11) NOT NULL,
    fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fecha_ultima_edicion TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE CASCADE,
    FOREIGN KEY (actividad) REFERENCES Actividad_Usuarios(id) ON DELETE CASCADE
);

CREATE TABLE Perfil_Comentarios(
    id INT(11) NOT NULL AUTO_INCREMENT,
    autor INT(11) NOT NULL,
    perfil_usuario INT(11) NOT NULL,
    contenido TEXT NOT NULL,
    fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fecha_ultima_edicion TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY (autor) REFERENCES Usuarios(id) ON DELETE CASCADE,
    FOREIGN KEY (perfil_usuario) REFERENCES Usuarios(id) ON DELETE CASCADE
);

CREATE TABLE Libros_Generos(
  id INT(11) NOT NULL AUTO_INCREMENT,
  genero INT(11) NOT NULL,
  libro INT(11) NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (genero) REFERENCES Generos(id) ON DELETE CASCADE,
  FOREIGN KEY (libro) REFERENCES Libros(id) ON DELETE CASCADE
);

CREATE TABLE Personas_Roles_Libros(
  id INT(11) NOT NULL AUTO_INCREMENT,
  rol INT(11) NOT NULL,
  persona INT(11) NOT NULL,
  libro INT(11) NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (rol) REFERENCES Roles(id) ON DELETE CASCADE,
  FOREIGN KEY (libro) REFERENCES Libros(id) ON DELETE CASCADE,
  FOREIGN KEY (persona) REFERENCES Personas(id) ON DELETE CASCADE
);

CREATE TABLE Actividad_API(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario INT(11),
    id_libro INT(11),
    fecha_consulta TIMESTAMP NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (usuario) REFERENCES Usuarios(id) ON DELETE SET NULL,
    FOREIGN KEY (id_libro) REFERENCES Libros(id) ON DELETE SET NULL
);


CREATE TABLE Seguidores(
    id INT(11) NOT NULL AUTO_INCREMENT,
    usuario_seguidor INT(11) NOT NULL,
    usuario_seguido INT(11) NOT NULL,
    PRIMARY KEY (id),
	  FOREIGN KEY (usuario_seguidor) REFERENCES Usuarios(id) ON DELETE CASCADE,
    FOREIGN KEY (usuario_seguido) REFERENCES Usuarios(id) ON DELETE CASCADE
);

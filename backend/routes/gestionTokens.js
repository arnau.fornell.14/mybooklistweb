const express = require('express');
const mariadb = require('mariadb/callback');
const crypto = require('crypto');
const router = express.Router();
const jwt = require('jsonwebtoken');


const conn = mariadb.createConnection({
    host: '80.59.197.13',
    port: '61616',
    user: 'jonathan',
    password: 'super3',
    database: 'MyBookList'
});

//genera una clave con el usuario, su contraseña hasheada y la fecha de hoy para luego generar el token
function generarClave(id, _callback) {
    conn.query("SELECT usuario, contrasenya FROM Usuarios WHERE id = ?;", [id], (err, rows) => {

        clave = rows[0].usuario.toString() + rows[0].contrasenya.toString() + (new Date()).getTime();
        const hash = crypto.createHash('md5').update(clave).digest('hex');

        conn.query("UPDATE Usuarios SET clave = ? WHERE id = ?;", [hash, id]);
        _callback(hash);
    });
};


//comprobar si el token del usuario es valido
function comprobarToken(token, usuario, _callback) {
    resultado = false;
    conn.query("SELECT fecha_caducidad, token, usuario FROM Tokens WHERE token = ?;", [token], (err, rows) => {
        if (!rows[0]) {
            resultado = false;
        } else {
            if (new Date(rows[0].fecha_caducidad).getTime() < Date.now()) {
                resultado = false;
                generarClave(usuario, (hash) => {});
                conn.query('DELETE FROM Tokens WHERE usuario = ?', [usuario]);
            } else {
                if (rows[0].usuario = usuario) {
                    if (rows[0].token = token) {
                        resultado = true;
                    } else {
                        resultado = false;
                    }
                } else {
                    resultado = false;
                }
            };
        };
        _callback(resultado);
    });
};

//Comprobar si los campos al registrarse tienen el length correcto
function comprobarRegistro(nombre, apellidos, usuario, contrasenya, email, fecha_nacimiento) {
    if (nombre.length < 3 || nombre.length > 255) {
        return false;
    } else if (apellidos.length < 3 || apellidos.length > 255) {
        return false;
    } else if (usuario.length < 3 || usuario.length > 50) {
        return false;
    } else if (contrasenya.length < 3 || contrasenya.length > 50) {
        return false;
    } else if (email.length < 3 || email.length > 100) {
        return false;
    } else if (fecha_nacimiento.getFullYear() < (new Date()).getFullYear() - 130) {
        return false;
    } else if (fecha_nacimiento.getTime() > Date.now()) {
        return false;
    } else {
        return true;
    }
};

function getNombresEstados(estados, _callback) {
    var query = 'SELECT nombre FROM Estados WHERE id IN (';
    for (i = 0; i < estados.length; i++) {
        query = query + "" + estados[i];
        if (i + 1 < estados.length) {
            query = query + ",";
        }
    }
    query = query + ") ORDER BY id ASC;";
    conn.query(query, (err, rows) => {
        if (err) {
            //TODO ERROR
        } else if (Object.keys(rows).length === 0) {
            //TODO ERROR
        } else {
            var nomEstados = [];
            rows.forEach(row => {
                nomEstados.push(row.nombre);
            });
            _callback(nomEstados);
        }
    });

}

//Peticion para iniciar sesion
router.post('/autenticar', (req, res) => {
    if (req.headers['authentication']) {
        token = req.headers['authentication'];
        getUsuarioFromToken(token, (id) => {
            if (id) {
                comprobarToken(token, id, (valido) => {
                    if (valido) {
                        conn.query('SELECT usuario, imagen FROM Usuarios WHERE id = ?', [id], (err, rows) => {
                            const expiracionD = new Date(new Date().setHours(new Date().getHours() + 1));
                            res.status(200).json({
                                mensaje: 'Autenticación correcta',
                                token: token,
                                expiracion: new Date(expiracionD.setHours(expiracionD.getHours() + 2)),
                                id: id,
                                nombre: rows[0].usuario,
                                imagen: rows[0].imagen
                            });
                            conn.query('UPDATE Tokens SET fecha_caducidad = ? WHERE usuario = ?', [expiracionD, id]);
                        });
                    } else {
                        res.status(200).json({
                            mensaje: 'Erro de token'
                        });
                    }
                });
            } else {
                res.status(200).json({
                    mensaje: 'Erro de token'
                });
            }
        });
    } else {
        const passhashed = crypto.createHash('sha512').update(req.body.contrasenya + req.body.usuario[0]).digest('hex');
        conn.query('SELECT id, imagen FROM Usuarios WHERE usuario = ? AND contrasenya = ?', [req.body.usuario, passhashed], (err, rows1) => {
            if (Object.keys(rows1).length === 0) {
                res.status(200).json({
                    mensaje: 'Usuario o contraseña incorrectos. Si no tienes cuenta, registrate.',
                });
            } else {
                const id = {
                    id: rows1[0].id
                };
                generarClave(id.id, (clave) => {
                    if (typeof clave === 'undefined') {
                        res.status(200).json({
                            mensaje: 'Usuario o contraseña incorrectos.',
                        });
                    } else {
                        const payload = {
                            check: true
                        };
                        const token = jwt.sign(payload, clave, {});
                        const now = new Date();
                        const expiracionD = new Date(new Date().setHours(new Date().getHours() + 1));
                        conn.query('SELECT usuario token FROM Tokens WHERE usuario = ?', [id.id], (err, rows) => {
                            if (Object.keys(rows).length === 0) {
                                res.status(200).json({
                                    mensaje: 'Autenticación correcta',
                                    token: token,
                                    expiracion: new Date(expiracionD.setHours(expiracionD.getHours() + 2)),
                                    id: id.id,
                                    imagen: rows1[0].imagen
                                });
                                conn.query('INSERT INTO Tokens(token, usuario, fecha_emision, fecha_caducidad) VALUES(?,?,?,?)', [token, id.id, now, expiracionD]);
                            } else {
                                const tokenRegis = {
                                    token: rows[0].token
                                };
                                res.status(200).json({
                                    mensaje: 'Autenticación correcta',
                                    token: tokenRegis.token,
                                    expiracion: new Date(expiracionD.setHours(expiracionD.getHours() + 2)),
                                    id: id.id,
                                    imagen: rows1[0].imagen
                                });
                                conn.query('UPDATE Tokens SET fecha_caducidad = ? WHERE usuario = ?', [expiracionD, id.id]);
                            }
                        });
                    }
                });
            }
        });
    }
});

//devolver los datos del usuario
router.get('/perfil/:usuario', (req, res) => {
  conn.query('SELECT id, imagen, biografia, fecha_nacimiento FROM Usuarios WHERE usuario = ?', [req.params.usuario], (err, rows) => {
    if(Object.keys(rows).length === 0) {
      res.status(200).json({
        mensaje: 'Este usuario no existe'
      });
    }
    else {
      res.status(200).json({
        id: rows[0].id,
        imagen: rows[0].imagen,
        usuario: req.params.usuario,
        biografia:  rows[0].biografia,
        fecha_nacimiento:  rows[0].fecha_nacimiento
      });
    }
  });
});

//comprobar si es seguidor
router.post('/seguidor', (req, res) => {
  if(req.headers['authentication']) {
    const token = req.headers['authentication']
    getUsuarioFromToken(token, id => {
      comprobarToken(token, id, valido => {
        if(valido) {
          conn.query('SELECT * FROM Seguidores WHERE usuario_seguido = ? AND usuario_seguidor = ?', [req.body.seguido, id], (err, rows) => {
            //console.log(rows)
            if(!rows[0]) {
              res.status(200).json({ siguiendo: false });
            }
            else {
              res.status(200).json({ siguiendo: true });
            }
          });
        }
      })
    });
  }
});

//comenzar a seguir a otro usuario
router.post('/seguir', (req, res) => {
  if(req.headers['authentication']) {
    const token = req.headers['authentication']
    getUsuarioFromToken(token, id => {
      comprobarToken(token, id, valido => {
        if(valido) {
          conn.query('INSERT INTO Seguidores (usuario_seguido, usuario_seguidor) VALUES (?, ?)', [req.body.seguido, id], (err, rows) => {
            if(!err) {
              res.status(200).json({ mensaje: 'Siguiendo correctamente' });
            }
          });
        }
      })
    });
  }
});

//dejar de seguir a otro usuario
router.post('/dejarseguir', (req, res) => {
  if(req.headers['authentication']) {
    const token = req.headers['authentication']
    getUsuarioFromToken(token, id => {
      comprobarToken(token, id, valido => {
        if(valido) {
          conn.query('DELETE FROM Seguidores WHERE usuario_seguido = ? AND usuario_seguidor = ?', [req.body.seguido, id], (err, rows) => {
            if(!err) {
              res.status(200).json({ mensaje: 'Dejando de seguir correctamente' });
            }
          });
        }
      })
    });
  }
});

//conseguir el id del usuario a través de un token
function getUsuarioFromToken(token, _callback) {
    resultado = false;
    conn.query("SELECT usuario FROM Tokens WHERE token = ?;", [token], (err, rows) => {
        if (!rows[0]) {
            _callback(null);
        } else {
            _callback(rows[0].usuario);
        }
    });
}

//Peticion de registro de usuario
router.post('/registro', (req, res) => {
    const passhashed = crypto.createHash('sha512').update(req.body.contrasenya + req.body.usuario[0]).digest('hex');
    const dates = new Date(req.body.fecha_nacimiento);
    if (comprobarRegistro(req.body.nombre, req.body.apellidos, req.body.usuario, req.body.contrasenya, req.body.email, dates)) {
        conn.query('INSERT INTO Usuarios (usuario, nombre, apellidos, email, contrasenya, imagen, fecha_nacimiento, biografia) VALUES (?,?,?,?,?,?,?,?)',
            [req.body.usuario, req.body.nombre, req.body.apellidos, req.body.email, passhashed, (req.body.imagen ? req.body.imagen : 'assets/img/MyBookList/imagen-perfil.jpg'), dates, req.body.biografia], (err, rows) => {
                if (err) {
                    res.status(200).json({
                        mensaje: 'Usuario o email ya registrados. Utiliza otros.'
                    });
                } else {
                    conn.query('SELECT id FROM Usuarios WHERE usuario = ? AND email = ?', [req.body.usuario, req.body.email], (err2, rows2) => {
                        if (Object.keys(rows2).length === 0) {
                            res.status(200).json({
                                mensaje: 'Error inesperado. No se encuentra el usuario designado.'
                            });
                        } else {
                            const id = {
                                id: rows2[0].id
                            };
                            conn.query('INSERT INTO Listas (id, usuario) VALUES (?,?)', [id.id, id.id], (err3, rows3) => {
                                if (err3) {
                                    res.status(200).json({
                                        mensaje: 'Error al crear la lista para el usuario. El usuario se debería haber registrado correctamente.'
                                    });
                                } else {
                                    res.status(200).json({
                                        mensaje: 'Registro correcto. Ya puedes iniciar sesión.'
                                    });
                                }
                            });
                        }
                    });
                }
            });
    } else {
        res.status(200).json({
            mensaje: 'Error de longitud de caracteres en algún campo.'
        });
    }
});

//conseguir los usuarios que sigues que tengan ese libro en su lista
router.post('/getSeguidoresFromLibro', (req, res) => {
  if(req.headers['authentication']) {
    token = req.headers['authentication'];
    getUsuarioFromToken(token, id => {
      comprobarToken(token, id, valido => {
        if(valido) {
          conn.query(`SELECT usuario FROM Usuarios INNER JOIN Seguidores ON Usuarios.id = Seguidores.usuario_seguido INNER JOIN Listas_Libros_Estados ON Usuarios.id = Listas_Libros_Estados.id_lista INNER JOIN Libros ON Listas_Libros_Estados.id_libro = Libros.id WHERE Libros.ISBN = ? AND usuario_seguidor = ?;`, [+req.body.isbn, +id], (err,rows) =>{
            if(err){
              console.log(err);
            }
            else if(Object.keys(rows).length === 0){

            } else {
              const totalMenosUno = rows.length-1;
               res.status(200).json({
                primerUsuario: rows[0].usuario,
                total : totalMenosUno
              });
          }
          });
        }
      });
  });
}
});

//Peticion de devolver lista de libros de usuario deseado
router.get('/listaLibros/:usuario', (req, res) => {
    conn.query('SELECT id FROM Usuarios WHERE usuario = ?', [req.params.usuario], (usuErr, usuRows) => {
        if (usuErr) {
            //console.log(usuErr);
        } else if (Object.keys(usuRows).length === 0) {
            res.status(200).json({
                mensaje: 'Error. No hay ningun usuario con ese nombre de usuario.'
            });
        } else {
            const idUsuario = {
                id: usuRows[0].id
            };
            conn.query('SELECT id FROM Listas WHERE usuario = ?', [idUsuario.id], (err, rows) => {
                if (err) {
                    //TODO
                } else if (Object.keys(rows).length === 0) {
                    res.status(200).json({
                        mensaje: 'Error. No hay ningun usuario con esa ID.'
                    });
                } else {
                    conn.query('SELECT id FROM Listas WHERE usuario = ? AND publica = 1', [idUsuario.id], (err2, rows2) => {
                        if (err2) {
                            //TODO
                        } else if (Object.keys(rows2).length === 0) {
                            res.status(200).json({
                                mensaje: 'Error. Este usuario tiene la lista privada.'
                            });
                        } else {
                            const idLista = {
                                id: rows2[0].id
                            };
                            conn.query('SELECT id_libro, fecha_adicion, id_estado, puntuacion FROM Listas_Libros_Estados WHERE id_lista = ? ORDER BY id_libro ASC', [idLista.id], (err3, rows3) => {
                                if (err3) {
                                    //console.log(err3);
                                } else if (Object.keys(rows3).length === 0) {
                                    res.status(200).json({
                                        mensaje: 'Error. Seguramente este usuario no tiene ningun libro en su lista.'
                                    });
                                } else {
                                    const librosLista = [];
                                    const idEstados = []
                                    rows3.forEach(rows3 => {
                                        const listaParams = {
                                            id_libro: rows3.id_libro,
                                            fecha_adicion: rows3.fecha_adicion,
                                            id_estado: rows3.id_estado,
                                            puntuacion: rows3.puntuacion
                                        };
                                        librosLista.push(listaParams);
                                        if (!idEstados.includes(rows3.id_estado)) {
                                            idEstados.push(rows3.id_estado)
                                        }
                                    });
                                    getNombresEstados(idEstados, (nomEstados) => {
                                        var query = 'SELECT id, titulo_original, portada, isbn FROM Libros WHERE id IN (';
                                        for (i = 0; i < librosLista.length; i++) {
                                            query = query + "" + librosLista[i].id_libro;
                                            if (i + 1 < librosLista.length) {
                                                query = query + ",";
                                            }
                                        }
                                        query = query + ") ORDER BY id ASC;";

                                        conn.query(query, (err4, rows4) => {
                                            if (err4) {
                                                //TODO
                                            } else if (Object.keys(rows4).length === 0) {
                                                //TODO
                                            } else {
                                                const resultado = [];
                                                var index = 0;
                                                rows4.forEach(row => {
                                                    const id = {
                                                        id: row.id
                                                    };
                                                    const libro = {
                                                        id_libro: librosLista[index].id_libro,
                                                        estado: librosLista[index].id_estado,
                                                        isbn: row.isbn,
                                                        titulo_original: row.titulo_original,
                                                        portada: row.portada,
                                                        puntuacion: librosLista[index].puntuacion,
                                                    };
                                                    resultado.push(libro);
                                                    index++;
                                                });
                                                res.status(200).json({
                                                    usuario: req.params.usuario,
                                                    estados: nomEstados,
                                                    resultado: resultado
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

//borra el token del usuario para cerrar su sesión
router.post('/cerrarSesion', (req, res) => {
    const idUsuario = req.body.usuario;
    const token = req.body.token;
    let tokenValido;
    comprobarToken(req.headers['authentication'], idUsuario, (token) => {
        tokenValido = token;
        if (tokenValido) {
            generarClave(idUsuario, (hash) => {});
            conn.query('DELETE FROM Tokens WHERE usuario = ?', [idUsuario]);
            res.status(200).json({
                mensaje: 'Todo correcto'
            });
        }
    });
});

//Metodo cuando un usuario decide editar la informacion de un libro que tiene en su lista (estado y puntuacion)
router.post('/modificarLibroLista/', (req, res) => {
    if (req.headers['authentication']) {
        token = req.headers['authentication'];
        getUsuarioFromToken(token, (id) => {
            if (id) {
                comprobarToken(token, id, (valido) => {
                    if (valido) {
                      //Hacer select de estado y puntuación para saber si el valor nuevo que ha introducido el usuario son iguales que antes
                        conn.query('SELECT id_estado, puntuacion FROM Listas_Libros_Estados WHERE id_lista = ? AND id_libro = ?;', [+id, +req.body.id_libro], (err, rows) => {
                            if (err) {
                                res.status(200).json({
                                    mensaje: 'Error inesperado.'
                                });
                            } else if (Object.keys(rows).length === 0) {
                                res.status(200).json({
                                    mensaje: 'La consulta no ha devuelto nada'
                                });
                            } else {
                                const select_id_estado = rows[0].id_estado;
                                const select_puntuacion = rows[0].puntuacion;
                                if (+req.body.id_estado !== select_id_estado && +req.body.puntuacion !== select_puntuacion) {
                                    //Si el usuario cambia la puntuacion y el estado a la vez
                                    conn.query('UPDATE Listas_Libros_Estados SET id_estado = ?, puntuacion = ? WHERE id_lista = ? AND id_libro = ?;', [+req.body.id_estado, +req.body.puntuacion, +id, +req.body.id_libro], (err2, rows2) => {
                                        if (err2) {
                                            res.status(200).json({
                                                mensaje: 'No se ha podido actualizar el estado y la puntuacion del libro.'
                                            });
                                        } else {
                                          getPuntuacionLibro(+req.body.id_libro, (puntuacion) => {
                                            conn.query('UPDATE Libros SET puntuacion = ? WHERE id = ?;', [puntuacion, +req.body.id_libro]);
                                            conn.query('INSERT INTO Actividad_Usuarios (usuario, tipo_actividad, id_libro) VALUES(?,?,?),(?,?,?);', [+id, +req.body.id_estado, +req.body.id_libro, +id, 6, +req.body.id_libro], (err3, rows3) => {
                                              if (err3) {
                                                  res.status(200).json({
                                                      mensaje: 'Se han actualizado el estado y la puntuacion del libro, pero no se ha generado actividad.'
                                                  });
                                              } else {
                                                  res.status(200).json({
                                                      mensaje: 'Se han actualizado el estado y la puntuacion del libro y se ha generado la actividad.'
                                                  });
                                              }
                                          });
                                          });

                                        }
                                    });
                                } else if (+req.body.id_estado !== select_id_estado) {
                                    //Si el usuario solo cambia el estado
                                    conn.query('UPDATE Listas_Libros_Estados SET id_estado = ? WHERE id_lista = ? AND id_libro = ?;', [+req.body.id_estado, +id, +req.body.id_libro], (err2, rows2) => {
                                        if (err2) {
                                            res.status(200).json({
                                                mensaje: 'No se ha podido actualizar el estado del libro.'
                                            });
                                        } else {
                                            conn.query('INSERT INTO Actividad_Usuarios (usuario, tipo_actividad, id_libro) VALUES(?,?,?);', [+id, +req.body.id_estado, +req.body.id_libro], (err3, rows3) => {
                                                if (err3) {
                                                    res.status(200).json({
                                                        mensaje: 'Se han actualizado el estado del libro, pero no se ha generado actividad.'
                                                    });
                                                } else {
                                                    res.status(200).json({
                                                        mensaje: 'Se han actualizado el estado del libro y se ha generado la actividad.'
                                                    });
                                                }
                                            });
                                        }
                                    });
                                } else if (+req.body.puntuacion !== select_puntuacion) {
                                    //Si el usuario solo cambia la puntuación
                                    conn.query('UPDATE Listas_Libros_Estados SET puntuacion = ? WHERE id_lista = ? AND id_libro = ?;', [+req.body.puntuacion, +id, +req.body.id_libro], (err2, rows2) => {
                                        if (err2) {
                                            res.status(200).json({
                                                mensaje: 'No se ha podido actualizar la puntuacion del libro.'
                                            });
                                        } else {
                                          getPuntuacionLibro(+req.body.id_libro, (puntuacion) =>{
                                            conn.query('UPDATE Libros SET puntuacion = ? WHERE id = ?;', [puntuacion, +req.body.id_libro]);
                                            conn.query('INSERT INTO Actividad_Usuarios (usuario, tipo_actividad, id_libro) VALUES(?,?,?);', [+id, 6, +req.body.id_libro], (err3, rows3) => {
                                              if (err3) {
                                                  res.status(200).json({
                                                      mensaje: 'Se han actualizado la puntuacion del libro, pero no se ha generado actividad.'
                                                  });
                                              } else {
                                                  res.status(200).json({
                                                      mensaje: 'Se han actualizado la puntuacion del libro y se ha generado la actividad.'
                                                  });
                                              }
                                          });
                                          });
                                        }
                                    });
                                } else {
                                  //Si el usuario introduze exactamente los mismos datos que tenia antes
                                    res.status(200).json({
                                        mensaje: 'No se ha actualizado nada.'
                                    });
                                }
                            }
                        });
                    } else {
                      //Si el usuario tiene un token no válido
                        res.status(200).json({
                            mensaje: 'Error de token'
                        });
                    }
                });
            } else {
              //Error al conseguir id de usuario a partir de token
                res.status(200).json({
                    mensaje: 'Error de token'
                });
            }
        });
    };
});

//borra un libro de tu lista
router.post('/borrarLibroLista', (req, res) => {
    if (req.headers['authentication']) {
        token = req.headers['authentication'];
        getUsuarioFromToken(token, (id) => {
            if (id) {
                comprobarToken(token, id, (valido) => {
                    if (valido) {
                        conn.query('DELETE FROM Listas_Libros_Estados WHERE id_lista = ? AND id_libro = ?', [+id, +req.body.id_libro]);
                        res.status(200).json({
                            mensaje: 'Borrado correctamente.'
                        });
                    } else {
                        res.status(200).json({
                            mensaje: 'Error de token'
                        });
                    }
                });
            } else {
                res.status(200).json({
                    mensaje: 'Error de token'
                });
            }
        });
    };
});

//genera un nuevo analisis de un libro válido
router.post('/crearAnalisis', (req, res) => {
    if (req.headers['authentication']) {
        token = req.headers['authentication'];
        getUsuarioFromToken(token, (id) => {
            if (id) {
                comprobarToken(token, id, (valido) => {
                    if (valido) {
                        conn.query('INSERT INTO Analisis (usuario, id_libro, contenido) VALUES (?,?,?);', [+id, req.body.id_libro, req.body.contenido], (err, rows) => {
                            if (err) {
                                res.status(200).json({
                                    mensaje: 'Error al insertar analisis en la base de datos.'
                                });
                            } else {
                                conn.query('INSERT INTO Actividad_Usuarios(usuario, tipo_actividad, id_libro) VALUES(?,7,?)', [+id, req.body.id_libro], (err3, rows3) => {
                                    if (err3) {
                                        res.status(200).json({
                                            mensaje: 'Error al insertar actividad, pero el analisis se ha añadido correctamente.'
                                        });
                                    } else {
                                        res.status(200).json({
                                            mensaje: 'Libro y actividad añadida.'
                                        });
                                    }
                                });
                            }
                        });

                    } else {
                        res.status(200).json({
                            mensaje: 'Error de token'
                        });
                    }
                });
            } else {
                res.status(200).json({
                    mensaje: 'Error de token'
                });
            }
        });
    };
});

//modifica los datos del perfil del usuario
router.post('/actualizarPerfil', (req, res) => {
  if (req.headers['authentication']) {
    token = req.headers['authentication'];
    getUsuarioFromToken(token, (id) => {
      if (id) {
        comprobarToken(token, id, (valido) => {
          if (valido) {
            conn.query('UPDATE Usuarios SET imagen = ?, biografia = ? WHERE id = ?;', [req.body.imagen, req.body.biografia, id], (err, rows2) => {
              if (err) {
                  res.status(200).json({
                      mensaje: 'No se ha podido actualizar la imagen y la biografia del usuario.'
                  });
              } else {
                res.status(200).json({mensaje: 'Se ha actualizado correctamente.'});
              }
            });
          } else {
            res.status(200).json({mensaje: 'Token invalido'});
          }
        });
      }
    });
  }
});

//mostrar el análisis creado anteriormente
router.post('/verAnalisis', (req, res) => {
    conn.query('SELECT Usuarios.id, Usuarios.usuario, Usuarios.imagen, Analisis.fecha_creacion, Analisis.fecha_ultima_edicion, contenido FROM Analisis JOIN Usuarios ON Analisis.usuario = Usuarios.id WHERE id_libro = ?;', [req.body.id_libro], (err, rows) => {
        if (err) {
            res.status(200).json({
                mensaje: 'Error inesperado'
            });
        } else {
            const librosLista = [];
            rows.forEach(row => {
                const listaParams = {
                    id_usuario: row.id,
                    usuario: row.usuario,
                    imagen: row.imagen,
                    fecha_creacion: row.fecha_creacion,
                    fecha_ultima_edicion: row.fecha_ultima_edicion,
                    contenido: row.contenido
                };
                librosLista.push(listaParams);
            });
            res.status(200).json({
                resultado: librosLista
            });
        }
    });
});

//ver las actividades de un usuario
router.post('/getActividad', (req, res) => {
  if(req.headers['authentication']) {
    token = req.headers['authentication'];
    getUsuarioFromToken(token, id => {
      comprobarToken(token, id, valido => {
        if(valido) {
          conn.query(`SELECT Actividad_Usuarios.id 'id', Usuarios.usuario 'nombre_usuario', Actividad_Usuarios.tipo_actividad 'actividad_id', Tipos_Actividad_Usuarios.nombre 'actividad',
          Libros.titulo_ingles 'titulo', Listas_Libros_Estados.puntuacion 'puntuacion',
          Libros.isbn 'isbn' FROM Actividad_Usuarios
          INNER JOIN Tipos_Actividad_Usuarios ON tipo_actividad = Tipos_Actividad_Usuarios.id
          INNER JOIN Usuarios ON Actividad_Usuarios.usuario = Usuarios.id
          INNER JOIN Libros ON Libros.id = Actividad_Usuarios.id_libro LEFT JOIN Listas_Libros_Estados ON Listas_Libros_Estados.id_libro = Libros.id AND Listas_Libros_Estados.id_lista = Usuarios.id
          INNER JOIN Seguidores ON Seguidores.usuario_seguido = Actividad_Usuarios.usuario WHERE Seguidores.usuario_seguidor = ?
          ORDER BY Actividad_Usuarios.fecha_creacion DESC
          LIMIT 3`, [id], (err, rows) => {
            if(rows[0]) {
              const actividades = []
              rows.forEach(row => {
                const actividad = {
                  id: row.id,
                  usuario: row.nombre_usuario,
                  actividad_id: row.actividad_id,
                  actividad: row.actividad,
                  titulo: row.titulo,
                  puntuacion: row.puntuacion,
                  isbn: row.isbn
                };
                actividades.push(actividad);
              });
              res.status(200).json({
                resultado : actividades
              });
            }
            else {
              res.status(200).json({mensaje: 'No hay actividad'});
            }
          });
        }
        else {
          res.status(200).json({mensaje: 'Token invalido'});
        }
      })
    });
  }
  else {
    res.status(200).json({mensaje: 'No hay actividad'});
  }
});

//añadir libros a una lista de un usuario
router.post('/anadirLibroLista', (req, res) => {
    const idUsuario = req.body.usuario;
    const idLibro = req.body.id_libro;
    //console.log(idUsuario, idLibro);
    conn.query('SELECT id FROM Listas WHERE usuario = ?', [idUsuario], (err, rows) => {
        if (err) {
            res.status(200).json({
                mensaje: 'error inesperados'
            });
        } else if (Object.keys(rows).length === 0) {
            res.status(200).json({
                mensaje: '0 columnas.'
            });
        } else {
            const lista = rows[0].id;
            conn.query('INSERT INTO Listas_Libros_Estados(id_lista, id_libro, id_estado, puntuacion) VALUES(?,?,5,0.0)', [lista, idLibro], (err2, rows2) => {
                if (err2) {
                    res.status(200).json({
                        mensaje: 'ERROR. Seguramente ya tengas este libro en tu lista.'
                    });
                } else {
                    conn.query('INSERT INTO Actividad_Usuarios(usuario, tipo_actividad, id_libro) VALUES(?,5,?)', [idUsuario, idLibro], (err3, rows3) => {
                        if (err3) {
                            res.status(200).json({
                                mensaje: 'Error al insertar actividad, pero el libro se ha añadido a la lista.'
                            });
                        } else {
                            res.status(200).json({
                                mensaje: 'Libro y actividad añadida.'
                            });
                        }
                    });
                }
            });
        }
    });
});

//ver la puntuacion media que le han puesto los usuarios a un libro
function getPuntuacionLibro(id_libro, _callback){
  conn.query("SELECT AVG(puntuacion) as avg_puntuacion FROM Listas_Libros_Estados WHERE id_libro = ? GROUP BY id_libro;", [id_libro], (err, rows) => {
    if(err){
      console.log(err);
    }
    else if (Object.keys(rows).length === 0) {
      const notaPunt = 0;
      _callback(notaPunt);
  } else {
    const notaPunt = rows[0].avg_puntuacion;
    _callback(notaPunt);
  }
  });
}

module.exports = router;

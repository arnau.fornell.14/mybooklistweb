import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../models/usuario.model';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: Usuario;
  isLogin: boolean = false;
  siguiendo: boolean = false;
  usuarioLogueado: Usuario;
  mensaje: string = '';

  constructor(private usuarioService: UsuarioService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe( paramMap => {
      const usuario =  paramMap.get('usuario');
      this.mensaje = '';
      this.usuarioService.getUsuario(usuario).subscribe(respuesta1 => {
        this.usuario = respuesta1;
        this.usuarioService.getSeguidor(this.usuario.id).subscribe(respuesta => {
          this.siguiendo = respuesta.siguiendo;
        });
      });
    });

    this.usuarioService.usuarioLogin.subscribe( login => {
      this.isLogin = login;
      this.usuarioLogueado = this.usuarioService.usuario;
    });

    if(this.usuarioService.usuario) {
      this.isLogin = true;
      this.usuarioLogueado = this.usuarioService.usuario;
    }

    if(this.usuario && this.isLogin) {
      this.usuarioService.getSeguidor(this.usuario.id).subscribe(respusta => {
        this.siguiendo = respusta.siguiendo;
      });
    }
  }

  public parsearFecha(fecha: string): string {
    const fechaParseada: Date = new Date(fecha);

    const fechaFinal: string = fechaParseada.getDate() + '/' + (fechaParseada.getMonth() + 1) + '/' + fechaParseada.getFullYear();
    return fechaFinal;
  }

  seguir() {
    this.usuarioService.seguir(this.usuario.id).subscribe( respuesta1 => {
      this.usuarioService.getSeguidor(this.usuario.id).subscribe(respuesta => {
        this.siguiendo = respuesta.siguiendo;
        this.mensaje = 'Siguiendo al usuario';
      });
    });
  }

  dejarSeguir() {
    this.usuarioService.dejarSeguir(this.usuario.id).subscribe( respuesta1 => {
      this.usuarioService.getSeguidor(this.usuario.id).subscribe(respuesta => {
        this.siguiendo = respuesta.siguiendo;
        this.mensaje = 'Se ha dejado de seguir al usuario';
      });
    });
  }

  onEditarPerfil() {
    this.router.navigate(['editar'], {relativeTo: this.route})
  }

  onVerLista() {
    this.router.navigate(['lista'], {relativeTo: this.route});
  }
}

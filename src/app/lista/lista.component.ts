import { Component, OnInit } from '@angular/core';
import { Lista } from '../models/lista.model';
import { ListaService } from '../services/lista.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../models/usuario.model';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-listas',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  lista: Lista;
  usuario: string;
  estados: string[] = ['Leyendo', 'Completado', 'Pausado', 'Dejado', 'Planeado para leer'];
  estadoActual: number = 1;
  mostrarFormulario: boolean = false;
  form: FormGroup;
  mensaje: string = '';
  usuarioLogueado: Usuario;
  isLogin: boolean = false;

  constructor(private listaService: ListaService, public route: ActivatedRoute, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.isLogin = false;
    this.route.paramMap.subscribe( paramMap => {
      const usuario =  paramMap.get('usuario');
      this.mensaje = '';
      this.listaService.getLista(usuario).subscribe(respuesta => {
        this.lista = respuesta.resultado;
        this.usuario = usuario;
      });
      this.usuarioLogueado = this.usuarioService.usuario;
    });
    this.form = new FormGroup({
      puntuacion: new FormControl(null , { validators: [Validators.required] }),
      estado: new FormControl(null , { validators: [Validators.required] })
    });

    if(this.usuarioService.usuario) {
      this.isLogin = true;
    }
    else {
      this.isLogin = false;
    }
    if(!localStorage.getItem('token')) {
      this.isLogin = false;
    }

    this.usuarioService.usuarioLogin.subscribe( login => {
      this.usuarioLogueado = this.usuarioService.usuario;
      this.isLogin = login;
    });
  }

  // Comprobacion de los estados
  onEstado(estado: string) {
    if(estado === 'Planeado para leer') {
      this.estadoActual = 5;
    }
    if(estado === 'Leyendo') {
      this.estadoActual = 1;
    }
    if(estado === 'Completado') {
      this.estadoActual = 2;
    }
    if(estado === 'Pausado') {
      this.estadoActual = 3;
    }
    if(estado === 'Dejado') {
      this.estadoActual = 4;
    }
    this.mensaje = ''
  }

  // Crear un array numerico y devolverlo.
  counter(i: number) {
    return new Array(i);
  }

  onSubmit(id_libro: number) {
    this.listaService.modificarLibroLista(id_libro, this.form.get('estado').value, this.form.get('puntuacion').value).subscribe( () => {
      this.route.paramMap.subscribe( paramMap => {
        const usuario =  paramMap.get('usuario');
        this.listaService.getLista(usuario).subscribe(respuesta => {
          this.lista = respuesta.resultado;
          this.usuario = usuario;
          this.mensaje = 'Libro modificado correctamente';
        });
      });
    });
  }

  // Metodo para eliminar un libro de la lista.
  onEliminar(id_libro: number) {
    this.listaService.eliminarLibroLista(id_libro).subscribe( () => {
      this.route.paramMap.subscribe( paramMap => {
        const usuario =  paramMap.get('usuario');

        this.listaService.getLista(usuario).subscribe(respuesta => {
          this.lista = respuesta.resultado;
          this.usuario = usuario;
          this.mensaje = 'Libro eliminado correctamente';
        });
      });
    });
  }

  resetForm() {
    this.form.reset({});
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actividad } from '../models/actividad.model';

@Injectable({providedIn: 'root'})
export class ActividadService {

  constructor(private http: HttpClient) {}

  getActividad() {
      return this.http.post<{ resultado: Actividad }>('http://localhost:3000/user/getActividad/', null);
  }
}

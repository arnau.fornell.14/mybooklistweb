import { Component, OnInit } from '@angular/core';
import { Libro } from '../models/libro.model';
import { LibrosService } from '../services/libros.service';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { AnalisisService } from '../services/analisis.service';

@Component({
  selector: 'app-ficha-libro',
  templateUrl: './ficha-libro.component.html',
  styleUrls: ['./ficha-libro.component.css']
})
export class FichaLibroComponent implements OnInit {

  libro: Libro;
  btnEnabled: boolean = true;
  mensaje: string = '';

  constructor(private libroService: LibrosService, public route: ActivatedRoute, private usuarioService: UsuarioService, private analisisService: AnalisisService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe( paramMap => {
      const isbn =  paramMap.get('isbn')
      this.mensaje = '';
      this.libroService.getLibro(isbn).subscribe(respuesta => {
        this.libro = respuesta.resultado;
        const ymd = this.libro.fecha_publicacion.split('T');
        const ymdSplit = ymd[0].split('-');
        this.libro.fecha_publicacion = ymdSplit[2]+"-"+ymdSplit[1]+"-"+ymdSplit[0];
        this.btnEnabled = respuesta.valido;
        this.analisisService.recargarAnalisis.next(this.libro.id);
      });
      this.usuarioService.usuarioLogin.subscribe(login => {
        this.libroService.getLibro(isbn).subscribe(respuesta => {
          this.btnEnabled = respuesta.valido;
        });
      })
    });
  }

  // Metodo para añadir un libro a la lista del usuario
  onAnyadirLibro() {
    this.libroService.anyadirLibroLista(this.usuarioService.usuario.id ,this.libro.id).subscribe((respuesta) => {
      this.btnEnabled = false;
      this.mensaje = respuesta.mensaje;
    });
  }
}

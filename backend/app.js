const express = require('express');
const bodyParser = require('body-parser');
const mariadb = require('mariadb/callback');
const jwt = require('jsonwebtoken');
const lineReader = require('line-reader');
const fetch = require("node-fetch");

const gestionTokens = require('./routes/gestionTokens');
const gestionApi = require('./routes/api.js');
const app = express();

const conn = mariadb.createConnection({
  host: '80.59.197.13',
  port: '61616',
  user:'jonathan',
  password: 'super3',
  database: 'MyBookList'
});

app.use(bodyParser.urlencoded({ extended: true }));

app.use((bodyParser.json()));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authentication');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
  next();
});

//devuelve cuatro libros de la base de datos
app.get('/getlibros', (req, res, next) => {
  conn.query("SELECT l.id, l.isbn, l.titulo_original, l.portada, COUNT(*) as total FROM Libros l INNER JOIN Listas_Libros_Estados lle ON l.id = lle.id_libro GROUP BY (l.id) ORDER BY total DESC LIMIT 4;", (err, rows) => {
    const resultado = [];
    rows.forEach(row => {
      const libro = {id:row.id, isbn: row.isbn, titulo_original: row.titulo_original, portada: row.portada};
      resultado.push(libro);
    });

    res.status(200).json({resultado: resultado});
  });
});

//se ejecuta cada vez que alguien escribe una letra en el buscador, sirve para mostrar una lista de sugerencias
app.post('/busqueda', (req, res) => {
  const busqueda = "%" + req.body.busqueda.toUpperCase() + "%";
  console.log(busqueda);
  conn.query(`(SELECT usuario 'nombre', imagen 'imagen', null 'isbn', 1 'tipo'FROM Usuarios
              WHERE UPPER(usuario) LIKE ? LIMIT 6)
              UNION
              (SELECT titulo_ingles 'nombre', portada 'imagen', isbn, 2 'tipo' FROM Libros
              WHERE UPPER(titulo_ingles) LIKE ? LIMIT 6)`, [busqueda, busqueda], (err, rows)=>{
                if(err){

                } else if (Object.keys(rows).length === 0){

                } else {
                  const linia = [];
                  rows.forEach(row => {
                    const select = {
                      nombre: row.nombre,
                      imagen: row.imagen,
                      isbn: row.isbn,
                      tipo: row.tipo
                    }
                    linia.push(select);
                  });
                  res.status(200).json({
                    resultado: linia
                  })
                }
              });
});

//consigue las novedades
app.get('/getnovedades', (req, res, next) => {
  conn.query("SELECT * FROM Libros ORDER BY fecha_publicacion LIMIT 4;", (err, rows) => {
    const resultado = [];

    rows.forEach(row => {
      const libro = {id:row.id, isbn: row.isbn, titulo_original: row.titulo_original, portada: row.portada};
      resultado.push(libro);
    });

    res.status(200).json({resultado: resultado});
  });
});

//conseguir la ficha de un libro a través de su isbn
app.get('/getfichalibro/:isbn', (req, res, next) => {
  conn.query(`SELECT Libros.id, Libros.titulo_ingles, Libros.titulo_original, Libros.isbn, Libros.portada, Libros.fecha_publicacion, Libros.sinopsis, Editoriales.nombre AS nombre_editorial, Roles.nombre AS nombre_rol, Personas.nombre AS nombre_persona, Personas.apellidos AS apellido_persona
              FROM Libros
              INNER JOIN Editoriales ON Libros.editorial = Editoriales.id
              INNER JOIN Personas_Roles_Libros ON Libros.id = Personas_Roles_Libros.libro
              INNER JOIN Roles ON Personas_Roles_Libros.rol = Roles.id
              INNER JOIN Personas ON Personas_Roles_Libros.persona = Personas.id
              WHERE isbn = ?;`, [req.params.isbn], (err, rows) => {

    getPuntuacionYTotalLibro(rows[0].id, (notaPunt) => {
      const libro = {id:rows[0].id, titulo_ingles:rows[0].titulo_ingles, titulo_original: rows[0].titulo_original, isbn: rows[0].isbn, portada: rows[0].portada, fecha_publicacion:rows[0].fecha_publicacion, sinopsis: rows[0].sinopsis, editorial: rows[0].nombre_editorial, puntuacion : notaPunt.puntuacion, total:notaPunt.total};



    rows.forEach(row => {
      libro[row.nombre_rol.toString().toLowerCase()] = row.nombre_persona + "" + (row.apellido_persona == null? "" : " "+row.apellido_persona);

    });
    const token = req.headers['authentication'];
    if(token) {
      getUsuarioFromToken(token, (id)=> {
        comprobarToken(token, id, (valido) => {
          if(valido){
            comprobarSiEnLista(id, rows[0].id, (enLista) =>{
              res.status(200).json({resultado: libro, valido: enLista});
            });
          }
        });
      });
    }
    else {
      res.status(200).json({resultado: libro, valido: false});
    }
    });
  });
});

//comprobar si un libro ya está en la lista del usuario
function comprobarSiEnLista(id_usuario, id_libro, _callback){
  conn.query("SELECT * FROM Listas_Libros_Estados WHERE id_lista = ? AND id_libro = ?", [id_usuario, id_libro], (err, rows) => {
    if(err){
      //ERROR INESPERADO
    }
    else if(Object.keys(rows).length === 0){
      _callback(true);
    } else{
      _callback(false);
    }
  });
}

//conseguir los datos de un libro
function getPuntuacionYTotalLibro(id_libro, _callback){
  conn.query("SELECT AVG(puntuacion) as avg_puntuacion, COUNT(*) as total FROM Listas_Libros_Estados WHERE id_libro = ? GROUP BY id_libro;", [id_libro], (err, rows) => {
    if(err){
      console.log(err);
    }
    else if (Object.keys(rows).length === 0) {
      const notaPunt = {puntuacion: 0, total: 0};
      _callback(notaPunt);
  } else {
    const notaPunt = {puntuacion: rows[0].avg_puntuacion, total: rows[0].total};
    _callback(notaPunt);
  }
  });
}

//conseguir un usuario a través de su token
function getUsuarioFromToken(token, _callback) {
  resultado = false;
    conn.query("SELECT usuario FROM Tokens WHERE token = ?;", [token], (err, rows) => {
      if (!rows[0]) {
        _callback(null);
      } else {
        _callback(rows[0].usuario);
      }
  });
}

//comprueba si el token sigue siendo válido
function comprobarToken(token, usuario, _callback) {
  resultado = false;
  conn.query("SELECT fecha_caducidad, token, usuario FROM Tokens WHERE token = ?;", [token], (err, rows) => {
      if (!rows[0]) {
          resultado = false;
      } else {
          if (new Date(rows[0].fecha_caducidad).getTime() < Date.now()) {
              resultado = false;
              generarClave(usuario, (hash) => {});
              conn.query('DELETE FROM Tokens WHERE usuario = ?', [usuario]);
          } else {
              if (rows[0].usuario = usuario) {
                  if (rows[0].token = token) {
                      resultado = true;
                  } else {
                      resultado = false;
                  }
              } else {
                  resultado = false;
              }
          };
      };
      _callback(resultado);
  });
};

//genera la clave del usuario a través de su usuario, su contraseña hasheada y la fecha de hoy en milisegundos.
function generarClave(id, _callback) {
  conn.query("SELECT usuario, contrasenya FROM Usuarios WHERE id = ?;", [id], (err, rows) => {

      clave = rows[0].usuario.toString() + rows[0].contrasenya.toString() + (new Date()).getTime();
      const hash = crypto.createHash('md5').update(clave).digest('hex');

      conn.query("UPDATE Usuarios SET clave = ? WHERE id = ?;", [hash, id]);
      _callback(hash);
  });
};

app.use('/user',gestionTokens);
app.use('/api', gestionApi);
module.exports = app;

//middleware

/*const rutasProtegidas = express.Router();
rutasProtegidas.use((req, res, next) => {
    const token = req.headers['access-token'];

    if (token) {
      jwt.verify(token, app.get('clave'), (err, decoded) => {
        if (err) {
          return res.json({ mensaje: 'Token inválida' });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.send({
          mensaje: 'Token no proveída.'
      });
    }
 });*/

 /*app.get('/autenticar2', rutasProtegidas, (req, res) => {
  res.status("200").json({mensaje:"Todo perfe"});
})*/


module.exports = app;

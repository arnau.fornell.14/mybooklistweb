export interface LibroSeguidores {
  primerUsuario: string;
  total: number;
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { AppComponent } from './app.component';
import { BarraComponent } from './barra/barra.component';
import { BusquedaComponent } from './barra/busqueda/busqueda.component';
import { LoginComponent } from './auth/login/login.component';
import { FichaLibroComponent } from './ficha-libro/ficha-libro.component';
import { InicioComponent } from './inicio/inicio.component';
import { AppRoutingModule } from './app-routing.module';
import { CarouselLibrosComponent } from './inicio/carousel-libros/carousel-libros.component';
import { RegistroComponent } from './auth/registro/registro.component';
import { ListaComponent } from './lista/lista.component';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { AnalisisComponent } from './ficha-libro/analisis/analisis.component';
import { EditarAnalisisComponent } from './ficha-libro/editar-analisis/editar-analisis.component';
import { ActividadLateralComponent } from './actividad-lateral/actividad-lateral.component';
import { PerfilComponent } from './perfil/perfil.component';
import { LibroSeguidoresComponent } from './ficha-libro/libro-seguidores/libro-seguidores.component';
import { EditarPerfilComponent } from './perfil/editar-perfil/editar-perfil.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    BarraComponent,
    BusquedaComponent,
    LoginComponent,
    FichaLibroComponent,
    InicioComponent,
    CarouselLibrosComponent,
    RegistroComponent,
    ListaComponent,
    AnalisisComponent,
    EditarAnalisisComponent,
    ActividadLateralComponent,
    PerfilComponent,
    LibroSeguidoresComponent,
    EditarPerfilComponent,
    FooterComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

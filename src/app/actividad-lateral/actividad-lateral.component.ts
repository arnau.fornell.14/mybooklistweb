import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { ActividadService } from '../services/actividad.service';
import { Actividad } from '../models/actividad.model';

@Component({
  selector: 'app-actividad-lateral',
  templateUrl: './actividad-lateral.component.html',
  styleUrls: ['./actividad-lateral.component.css']
})
export class ActividadLateralComponent implements OnInit {

  isLogin: boolean;
  actividad: Actividad;

  constructor(private usuarioService: UsuarioService, private actividadService: ActividadService) { }

  ngOnInit(): void {
    this.isLogin = this.usuarioService.usuario? true: false; // Comprobacion para saber si el usuario se encuentra logueado.
    if(this.isLogin) {
      this.actividadService.getActividad().subscribe( resultado => {
        this.actividad = resultado.resultado;
      });
    }
  }
}
